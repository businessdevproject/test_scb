const os = require('os');
const config = require('./config/api');
process.env.NODE_ENV = config.NODE_ENV;
process.env.LISTENPORT = config.LISTENPORT;
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');
const {spawn} = require('child_process');
const path = require('path');

// Configure chai
chai.use(chaiHttp);
chai.should();


describe('API Server', function () {
    let server;
    this.timeout(20000);

    after(() => {
        server.kill('SIGINT');
    });

    it('should start server',async () => {

        await new Promise((resolve) => {
            server = spawn('node', [path.resolve(__dirname + '/../', 'api.js')], {env: {LISTENPORT: process.env.LISTENPORT, NODE_ENV: process.env.NODE_ENV, PATH: process.env.PATH}});
            server.stdout.on('data', function (data) {
                let line = data.toString();
                console.log('SERVER:', line);
                if (line.indexOf('port') !== -1) {
                    resolve();
                }
            });
            server.stderr.on('data', function (data) {
                process.stderr.write(data);
            });
        });
    });
});

describe('Call SDK', function () {
    const TestScb = require('../sdk/index');
    let testScb = new TestScb();
    before(async () => {
        await testScb.init('test_scb', 'TEST')
    });

    after(async () => {
        testScb.shutdown();
    });
    it("should login case null", async() => {
        const result = await testScb.user.login({
            username: "string",
            password: "string"
        })
        expect(result).to.be.a('null');
    });

    it("should login case success", async() => {
        await testScb.user.create({
            "username": "string.string",
            "password": "string",
            "date_of_birth": "string"
          });
        const result = await testScb.user.login({
            username: "string.string",
            password: "string"
        })
        expect(result.name).to.be.a('string');
    });

    it("should findAll", async() => {
        const result = await testScb.user.findAll();
        expect(result).to.have.lengthOf(1);
    })

    it("should order", async() => {
        let result = await testScb.user.login({
            username: "string.string",
            password: "string"
        })
        await testScb.user.order([1,2,3], result.id);
        result = await testScb.user.login({
            username: "string.string",
            password: "string"
        })
        expect(result.book).to.deep.equal([1, 2, 3]);
    })
    
    it("should delete", async() => {
        let result = await testScb.user.login({
            username: "string.string",
            password: "string"
        })
        await testScb.user.delete(result.id);
        result = await testScb.user.login({
            username: "string.string",
            password: "string"
        })
        console.log("resultresult",result)
        expect(result).to.be.a('null');
    })
})