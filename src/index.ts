const Sdk = require('@sabuytech/libsbt');
const _ = require('lodash');
const config = require('./config/' + process.env.NODE_ENV);
const db = require("../models");
const axios = require('axios');

import User from './User';
/**
 * @class Atlantic
 * @extends Sdk
 */
export default class TestScb extends Sdk {

    /**
     * @constructor
     */
    constructor() {
        super();
        this.config = config;
    }

    /**
     * Init Sale Class
     * @returns {Promise<Atlantic>}
     */
    async init(origin: string, facility: string) {
        await super.init(origin, facility, false);
        this.db = db;
        this.user = await new User(this);

        this.otherApi = axios.create({
            baseURL: config.BOOK_PUBLISHER_PATH,
        });

        return this;
    }

    shutdown() {
        super.shutdown();
    }
}

module.exports = TestScb;
