import TestScb from "./index";
import IDefaultDao from "./interface/DefaultInterface";

export default class User implements IDefaultDao {

    private testScb: TestScb;

    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(testScb: TestScb) {
        this.testScb = testScb;
    }

    
    public login(data: any): any {
        return this.testScb.db.user.findOne({ 
            where: data,
            attributes: ["id", "name", "surname", "date_of_birth", "book"]
        }).then((user: any) => {
            return JSON.parse(JSON.stringify(user));
        });
    }

    public findAll(): any {
        return this.testScb.db.user.findAll({
            attributes: ["id", "name", "surname", "date_of_birth", "book"],
        }).then((users: any) => {
            return JSON.parse(JSON.stringify(users));
        });
    }

    public delete(id: number): any {
        return this.testScb.db.user.destroy({
            where: { id: id }
        });
    }

    public order(orders: any, id: number): any {
        return this.testScb.db.user.update({
            book: orders,
        }, {
            where: { id: id }
        }).then(async (user: any) => {
            console.log(">> Order book: " + JSON.stringify(user));
            return user;
        });
    }

    public create(data: any): any {
        const myArr = data.username.split(".");
        return this.testScb.db.user.create({
            name: myArr[0],
            surname: myArr[1],
            date_of_birth: data.date_of_birth,
            book: [],
            username: data.username,
            password: data.password
        }).then(async (user: any) => {
            console.log(">> Created user: " + JSON.stringify(user));
            return user;
        });
    }
}