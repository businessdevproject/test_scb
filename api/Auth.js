module.exports.login = async (req, res, next) => {
    try {
        const result = await TESTSCB.user.login(req.body);
        if(result){
            res.status(200).send({
                result: result
            });
        }else{
            res.status(401).send({
                code: 10232,
                message: "username or password is incorrect"
            });
        }
    } catch (e) {
        next(e)
    }
};