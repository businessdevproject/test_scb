module.exports.get = async (req, res, next) => {
    try {
        const resBookRecommends = await TESTSCB.otherApi.get("books/recommendation")
        const resBooks = await TESTSCB.otherApi.get("books")
        let result = {};
        for(let bookRecommend of resBookRecommends.data){
            bookRecommend.is_recommended = true;
            result["key_"+bookRecommend.id] = bookRecommend;
            delete result["key_"+bookRecommend.id].id;
        }
        for(let book of resBooks.data){
            if(!result["key_"+book.id]){
                book.is_recommended = false;
                result["key_"+book.id] = book;
                delete result["key_"+book.id].id;
            }
        }
        res.status(200).send({
            book: Object.values(result)
        });
    } catch (e) {
        next(e)
    }
};