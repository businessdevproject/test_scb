module.exports.get = async (req, res, next) => {
    try {
        const result = await TESTSCB.user.findAll();
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};
module.exports.order = async (req, res, next) => {
    try {
        const result = await TESTSCB.user.order(req.body.orders, req.params.user_id);
        const resBooks = await TESTSCB.otherApi.get("books")
        let price = 0;
        let bookObj = {};
        for(let book of resBooks.data){
            bookObj[book.id] = book;
        }
        for(let order of req.body.orders){
            price += bookObj[order].price;
        }
        res.status(200).send({
            price: price
        });
    } catch (e) {
        next(e)
    }
};
module.exports.create = async (req, res, next) => {
    try {
        const result = await TESTSCB.user.create(req.body);
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};
module.exports.delete = async (req, res, next) => {
    try {
        const result = await TESTSCB.user.delete(req.params.user_id);
        res.status(200).send({
            result: result
        });
    } catch (e) {
        next(e)
    }
};