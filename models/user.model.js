module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("user", {
        name: {
            type: DataTypes.STRING,
        },
        surname: {
            type: DataTypes.STRING,
        },
        date_of_birth: {
            type: DataTypes.STRING,
        },
        book: {
            type: DataTypes.ARRAY(DataTypes.INTEGER),
        },
        username: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
    });
    return User;
};