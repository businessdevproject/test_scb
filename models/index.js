const config = require("../sdk/config/" + process.env.NODE_ENV);

const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.mysqlDb.DB, config.mysqlDb.USER, config.mysqlDb.PASSWORD, {
    host: config.mysqlDb.HOST,
    dialect: config.mysqlDb.dialect,
    operatorsAliases: false,
    logging: console.log,
    pool: {
        max: config.mysqlDb.pool.max,
        min: config.mysqlDb.pool.min,
        acquire: config.mysqlDb.pool.acquire,
        idle: config.mysqlDb.pool.idle,
    },
});
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("./user.model.js")(sequelize, Sequelize);

// fix for test
db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});
module.exports = db;