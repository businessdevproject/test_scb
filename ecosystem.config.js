module.exports = {
  apps : [{
    name      : 'test_scb',
    script    : 'api.js',
    log_date_format: "YYYY-MM-DD HH:mm:ss",
    merge_logs: true,
    env: {
      NODE_ENV: 'development',
      LISTENPORT: 9046
    },
    env_production : {
      NODE_ENV: 'production',
      LISTENPORT: 9046
    },
    env_production2 : {
      NODE_ENV: 'production2',
      LISTENPORT: 9046
    },
    env_uat : {
      NODE_ENV: 'uat',
      LISTENPORT: 9046
    }
  }],
  deploy : {
    production : {
      user : 'api',
      host : [
          'prod-svc3',
          'prod-svc4'
          ],
      ref  : 'origin/master',
      repo : 'git@gitlab.vdc.co.th:thomas/test_scb.git',
      path : '/home/api/test_scb',
      'pre-deploy' : 'git fetch --all',
      'post-deploy' : "npm install  && pm2 reload ecosystem.config.js --env production",
      "env": {
        "NODE_ENV": "production"
      }
    },
    "prod-svc3": {
      "user": "api",
      "host": [
        "prod-svc3"
      ],
      "ref": "origin/master",
      "repo": "git@gitlab.vdc.co.th:thomas/test_scb.git",
      "path": "/home/api/test_scb",
      "pre-deploy": "git fetch --all",
      "post-deploy": "npm install && pm2 reload ecosystem.config.js --env production",
      "env": {
        "NODE_ENV": "production"
      }
    },
    "prod-svc4": {
      "user": "api",
      "host": [
        "prod-svc4"
      ],
      "ref": "origin/master",
      "repo": "git@gitlab.vdc.co.th:thomas/test_scb.git",
      "path": "/home/api/test_scb",
      "pre-deploy": "git fetch --all",
      "post-deploy": "npm install  && pm2 reload ecosystem.config.js --env production",
      "env": {
        "NODE_ENV": "production"
      }
    },
    uat : {
      user : 'api3',
      host : 'uat-svc1',
      ref  : 'origin/uat',
      repo : 'git@gitlab.vdc.co.th:thomas/test_scb.git',
      path : '/home/api3/test_scb',
      'pre-deploy' : 'git fetch --all',
      'post-deploy' : 'npm install && npm run config:uat:deploy && npm run migrate -- -e uat && pm2 reload ecosystem.config.js --env uat && pm2 reload ecosystem-engine.config.js --env uat'
    },
    dev : {
      user : 'api3',
      host : 'dev-svc1',
      ref  : 'origin/dev',
      repo : 'git@gitlab.vdc.co.th:thomas/test_scb.git',
      path : '/home/api3/test_scb',
      'pre-deploy' : 'git fetch --all',
      'post-deploy' : 'npm install && npm run migrate -- -e development && pm2 reload ecosystem.config.js --env dev'
    }
  }
};
