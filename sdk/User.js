"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class User {
    /**
     * @constructor
     * @param {Atlantic} Atlantic SDK
     */
    constructor(testScb) {
        this.testScb = testScb;
    }
    login(data) {
        return this.testScb.db.user.findOne({
            where: data,
            attributes: ["id", "name", "surname", "date_of_birth", "book"]
        }).then((user) => {
            return JSON.parse(JSON.stringify(user));
        });
    }
    findAll() {
        return this.testScb.db.user.findAll({
            attributes: ["id", "name", "surname", "date_of_birth", "book"],
        }).then((users) => {
            return JSON.parse(JSON.stringify(users));
        });
    }
    delete(id) {
        return this.testScb.db.user.destroy({
            where: { id: id }
        });
    }
    order(orders, id) {
        return this.testScb.db.user.update({
            book: orders,
        }, {
            where: { id: id }
        }).then(async (user) => {
            console.log(">> Order book: " + JSON.stringify(user));
            return user;
        });
    }
    create(data) {
        const myArr = data.username.split(".");
        return this.testScb.db.user.create({
            name: myArr[0],
            surname: myArr[1],
            date_of_birth: data.date_of_birth,
            book: [],
            username: data.username,
            password: data.password
        }).then(async (user) => {
            console.log(">> Created user: " + JSON.stringify(user));
            return user;
        });
    }
}
exports.default = User;
//# sourceMappingURL=User.js.map