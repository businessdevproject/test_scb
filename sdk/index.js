"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sdk = require('@sabuytech/libsbt');
const _ = require('lodash');
const config = require('./config/' + process.env.NODE_ENV);
const db = require("../models");
const axios = require('axios');
const User_1 = require("./User");
/**
 * @class Atlantic
 * @extends Sdk
 */
class TestScb extends Sdk {
    /**
     * @constructor
     */
    constructor() {
        super();
        this.config = config;
    }
    /**
     * Init Sale Class
     * @returns {Promise<Atlantic>}
     */
    async init(origin, facility) {
        await super.init(origin, facility, false);
        this.db = db;
        this.user = await new User_1.default(this);
        this.otherApi = axios.create({
            baseURL: config.BOOK_PUBLISHER_PATH,
        });
        return this;
    }
    shutdown() {
        super.shutdown();
    }
}
exports.default = TestScb;
module.exports = TestScb;
//# sourceMappingURL=index.js.map