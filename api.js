const cors = require('cors');
const path = require('path');
const TestScb = require('./sdk');
const LISTENPORT = process.env.LISTENPORT;
const LANGUAGE = 'th-TH';
module.exports = (
    /**
     * @param {Auth} ATLAS
     * @param {TestScb} TESTSCB
     * @returns {Promise<express>}
     */
    async (TESTSCB) => {
        try {
            global.TESTSCB = TESTSCB;
            global.LOG_DB = false;

            process.on('SIGINT', function () {
                TESTSCB.shutdown();
                process.exit();
            });
            TESTSCB.setElasticSearchLog(TESTSCB.config.elasticsearch);
            await TESTSCB.init('test_scb', 'api');

            const options = {
                controllers: path.join(__dirname, './api'),
                loglevel: 'error',
                strict: true,
                router: true,
                validator: true,
                customErrorHandling: true,
                port: LISTENPORT,
                docs: {
                    apiDocs: '/api-doc',
                    apiDocsPrefix: '/test_scb',
                    swaggerUi: '/docs',
                    swaggerUiPrefix: '/test_scb',
                },
                expressJson: {
                    limit: '2mb'
                }
            };

            const app = await TESTSCB.initApi(__dirname + '/api.yaml', options);

            //add default language as query param to each request
            app.use((req, res, next) => {
                req.language = req.query.language || LANGUAGE;
                next();
            });

            app.use(cors());

            return await TESTSCB.setApiRoutes();
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    }
)(new TestScb());

