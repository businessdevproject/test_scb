module.exports = {
  apps : [{
    "node_args": ["--inspect=127.0.0.1:14146"],
    name      : 'test_scb',
    script    : 'api.js',
    log_date_format: "YYYY-MM-DD HH:mm:ss",
    merge_logs: true,
    "env": {
      "NODE_ENV": "development",
      "LISTENPORT": 14046
    }
  }]
};
