# install docker
1. Install docker https://docs.docker.com/get-docker/

# install postgresql database
1. docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=123456 -d postgres
2. docker exec -it postgresql-container psql -U postgres -c "CREATE DATABASE testdb"

# install service case clone project
1. docker build -t test-scb .
2. docker run -it -p 14046:14046 test-scb

# install service case git repositories
1. docker build -t test-scb github='https://romemint@bitbucket.org/businessdevproject/test_scb.git'
2. docker run -it -p 14046:14046 test-scb

# use docs api on
http://localhost:14046/test_scb/docs/#/