const DataModel = require('../libsbt/sdk/DataModel');

class GenerateModel {

    async init() {
        let dirPath = __dirname + '/sdk/schema/db/object';
        let toDirPath = dirPath.replace("/sdk/", "/src/");
        await DataModel.generateModel(dirPath, toDirPath);
    }
}

const generateModel = new GenerateModel();
generateModel.init();