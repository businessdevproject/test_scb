FROM node:14

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY ./ ./

RUN npm i pm2 -g

EXPOSE 14046
CMD ["npm", "run", "start-local"]